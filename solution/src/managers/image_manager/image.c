#include "image.h"

//image create
struct image image_create(uint32_t width, uint32_t height) {
    return (struct image){
            .width = width,
            .height = height,
            .data = (struct pixel *)malloc(sizeof(struct pixel) * width * height)};
}

//image free
void image_delete(struct image img) {
    free(img.data);
}
