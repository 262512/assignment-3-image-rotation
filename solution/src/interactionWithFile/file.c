#include "file.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

enum state open_f(FILE **f, char const *path, char const *mode) {
    *f = fopen(path, mode);
    return *f == NULL ? OPEN_ERROR : SUCCESS;
}

enum state close_f(FILE *f) {
    if (fclose(f) == 0) {
        return SUCCESS;
    } else {
        return CLOSE_ERROR;
    }
}
void freeFSpace(FILE **f1, FILE **f2) {
    free(f1);
    free(f2);
}
