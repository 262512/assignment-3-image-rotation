#include "transform.h"
#include "image.h"
#include <stdint.h>


struct image rotate_img(struct image const source) {
    struct image r_image = image_create(source.height, source.width);

    for (uint64_t i = 0; i < r_image.height; i++) {
        for (uint64_t j = 0; j < r_image.width; j++) {
            r_image.data[i * r_image.width + j] =
                    source.data[(source.height - j - 1) * source.width + i];
        }
    }
    return r_image;
}

