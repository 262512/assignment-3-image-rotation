#include "main.h"
#include "bmp.h"
#include "bmp_status.h"
#include "file.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>



int main(int argc, char **argv) {
    if (argc != 3) {
        puts("The amount of args must be 3.");
        return 1;
    }

    struct image Image;
    struct image ImageRotated;
    enum state fileStatusOpen;
    enum state fileStatusClos;

    FILE **fileIn = malloc(sizeof(FILE *));
    FILE **fileOut = malloc(sizeof(FILE *));

    fileStatusOpen = open_f(fileIn, argv[1], "rb");
    if (fileStatusOpen == SUCCESS) {
        puts("Input file open\n");
    } else {
        fprintf(stderr, "%s","input file open error");
        freeFSpace(fileIn, fileOut);
        return -1;
    }

    fileStatusOpen = open_f(fileOut, argv[2], "wb");
    if (fileStatusOpen == SUCCESS) {
        puts("Output file open\n");
    } else {
        fprintf(stderr, "%s","open error");
        freeFSpace(fileIn, fileOut);
        return -1;
    }

    enum read_status fileReadStatus = from_bmp(*fileIn, &Image);
    switch (fileReadStatus) {
        case BMP_READ_ERROR_IO:
            fprintf(stderr, "%s","read error bmp");
            break;
        case READ_BMP_ERROR_D:
            fprintf(stderr, "%s","read bmp error data");
            break;
        case READ_INVALID_BITS:
            fprintf(stderr, "%s","read invalid bits");
            break;
        case READ_BMP_ERROR_1:
            fprintf(stderr, "%s","read bmp error 1");
            break;
        case READ_BMP_ERROR_2:
            fprintf(stderr, "%s","read bmp error 2");
            break;
        case READ_INVALID_FILE_SIZE:
            fprintf(stderr, "%s","read invalid file size");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "%s","read invalid signature");
            break;
        case READ_INVALID_IMAGE_SIZE:
            fprintf(stderr, "%s","read invalid image size");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "%s","read invalid header");
            break;
        default:
            if(fileReadStatus == READ_BMP_OK)
                puts("BMP read success");
            else
                fprintf(stderr, "%s","something gone wrong?");
            break;
    }

    if (fileReadStatus != READ_BMP_OK) {
        freeFSpace(fileIn, fileOut);
        return -1;
    }

    ImageRotated = rotate_img(Image);
    puts("image rotated\n");


    image_delete(Image);

    enum write_status f_write_status = to_bmp(*fileOut, &ImageRotated);
    if (f_write_status == WRITE_BMP_OK) {
        puts("bmp is written");
    }
    else {
        image_delete(ImageRotated);
        freeFSpace(fileIn, fileOut);
        fprintf(stderr, "%s","bmp write error");
        return -1;
    }

    image_delete(ImageRotated);

    fileStatusClos = close_f(*fileIn);
    if (fileStatusClos == SUCCESS) {
        puts("Input file success\n");
    }

    else {
        fprintf(stderr, "%s","error file close");
        freeFSpace(fileIn, fileOut);
        return -1;
    }

    fileStatusClos = close_f(*fileOut);

    if (fileStatusClos == SUCCESS) {
        puts("Output file success\n");
    }

    else {
        fprintf(stderr, "%s","error file close");
        freeFSpace(fileIn, fileOut);
        return -1;
    }

    freeFSpace(fileIn, fileOut);

        return 0;
}
