#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H
#include "stdio.h"
#include "stdlib.h"
enum state {
    SUCCESS = 0,
    OPEN_ERROR,
    CLOSE_ERROR,
    READ_ERROR,
    WRITE_ERROR,
    CONVERT_ERROR
};

enum state open_f(FILE **f, char const *path, char const *mode);

enum state close_f(FILE *f);

void freeFSpace(FILE **f1, FILE **f2);

#endif

