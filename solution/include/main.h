#ifndef IMAGE_ROTATION_MAIN_H
#define IMAGE_ROTATION_MAIN_H

#include <stdio.h>

void free_f(FILE **f1, FILE **f2);
int main(int argc, char **argv);

#endif // !IMAGE_ROTATION_MAIN_H
